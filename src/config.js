/* eslint-disable no-unused-vars */
import path from 'path'
import {merge} from 'lodash'
import knexConfig from '../knexfile'

/* istanbul ignore next */
const requireProcessEnv = (name) => {
  if (!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable')
  }
  return process.env[name]
}

/* istanbul ignore next */
if (process.env.NODE_ENV !== 'production') {
  const dotenv = require('dotenv-safe')
  dotenv.load({
    path: path.join(__dirname, '../.env'),
    sample: path.join(__dirname, '../.env.example')
  })
}

const config = {
  all: {
    env: process.env.NODE_ENV || 'development',
    root: path.join(__dirname, '..'),
    port: process.env.PORT || 8888,
    ip: process.env.IP || '0.0.0.0',
    masterKey: requireProcessEnv('API_KEY'),
    jwtSecret: requireProcessEnv('JWT_SECRET'),
    salt: 8,
    postgres: {
      pool: {
        min: 2,
        max: 10
      },
      migrations: {
        tableName: 'knex_migrations'
      }
    }
  },
  mail: {
    test: {
      host: 'smtp.example.com',
      port: 465,
      from: '"Test 👻" <test@blurdybloop.com>',
      secure: true, // secure:true for port 465, secure:false for port 587
      auth: {
        user: 'username@example.com',
        pass: 'userpass'
      }
    },
    development: {
      host: 'smtp.example.com',
      port: 465,
      from: '"dev 👻" <dev@blurdybloop.com>',
      secure: true, // secure:true for port 465, secure:false for port 587
      auth: {
        user: 'username@example.com',
        pass: 'userpass'
      }
    },
    production: {
      host: 'smtp.example.com',
      port: 465,
      from: '"production 👻" <foo@blurdybloop.com>',
      secure: true, // secure:true for port 465, secure:false for port 587
      auth: {
        user: 'username@example.com',
        pass: 'userpass'
      }
    }
  },
  ...knexConfig
}

module.exports = merge(config.all, config[config.all.env], {mail: config.mail[config.all.env]})
export default module.exports
