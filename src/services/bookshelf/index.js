import { client, connection, pool } from '../../config'
import bookshelf from 'bookshelf'
import knex from 'knex'

const bookshelfClient =
  bookshelf(
    knex({
      client,
      connection: {
        ...connection

      },
      pool
    })
  )

export default bookshelfClient
