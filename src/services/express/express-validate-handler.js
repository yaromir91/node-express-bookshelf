import ev from 'express-validation'

export default function (err, req, res, next) {
  if (err instanceof ev.ValidationError) return res.status(err.status).json(err)
  if (process.env.NODE_ENV !== 'production') {
    return res.status(500).send(err.stack)
  } else {
    return res.status(500)
  }
}
