import { createTransport } from 'nodemailer'
import { mail } from '../../config'

const transport = createTransport(mail)

export const sendRegistration = (email) => {
  return transport.sendMail({
    from: mail.from,
    to: email,
    subject: 'Registration',
    test: 'Thx Registration!',
    html: '<h1>Thx Registration!</h1>'
  })
}

export default transport
