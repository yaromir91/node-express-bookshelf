import passport from 'passport'
import { Strategy as BearerStrategy } from 'passport-http-bearer'
import User from '../../api/users/model'

export const token = () => (req, res, next) =>
  passport.authenticate('token', { session: false }, (err, user) => {
    if (err || !user) {
      return res.status(401).end('Unauthorized')
    }
    req.logIn(user, { session: false }, (err) => {
      if (err) return res.status(401).end('Unauthorized')
      next()
    })
  })(req, res, next)

passport.use('token', new BearerStrategy((token, done) => {
  if (token) {
    new User({token}).fetch().then((user) => {
      if (user) {
        done(null, user)
      } else {
        done(null, {})
      }
    }).catch(done)
  } else {
    done(null, false)
  }
}))
