import bcrypt from 'bcrypt'
import co from 'co'
import bookshelf from '../../services/bookshelf'
import {ValidationError} from 'express-validation'
import {roles} from '../roles/model'
import {salt} from '../../config'

const Users = bookshelf.Model.extend({
  tableName: 'users'
}, {
  create: co.wrap(function *({email, password, role, address}) {
    const roleId = roles.filter(e => e.name === role.toLowerCase().trim() ? e.id : null)[0].id
    const user = yield new this({
      email: email.toLowerCase().trim(),
      password: generateBcrypt(password),
      token: generateBcrypt(email),
      role_id: roleId,
      address
    }).save()
    return user
  }),
  login: co.wrap(function *(email, password) {
    return new this({email: email.toLowerCase().trim()}).fetch().tap(function (user) {
      if (!user) throw new ValidationError('User not found', {status: 400, statusText: 'Bad Request'})
      return bcrypt.compare(password, user.get('password'))
        .then(function (res) {
          if (!res) throw new ValidationError('Invalid password', {status: 400, statusText: 'Bad Request'})
        })
    })
  })
})

export const generateBcrypt = (string) => bcrypt.hashSync(string, salt)

export default Users
