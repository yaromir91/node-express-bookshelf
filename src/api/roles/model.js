import bookshelf from '../../services/bookshelf'

export const roles = [
  {
    id: 1,
    name: 'admin'
  },
  {
    id: 2,
    name: 'consumer'
  },
  {
    id: 3,
    name: 'contractor'
  }
]

const Roles = bookshelf.Model.extend({
  tableName: 'roles'
})

export default Roles
