import co from 'co'
import {success} from '../../services/response/'
import User from '../users/model'

export const login = ({ body }, res, next) => co(function* () {
  const { email, password } = body
  const user = yield User.login(email, password)
  success(res)(user.omit(['password']))
}).catch(next)

export const registration = ({ body }, res, next) => co(function* () {
  const user = yield User.create(body)
  success(res)(user)
}).catch(next)
