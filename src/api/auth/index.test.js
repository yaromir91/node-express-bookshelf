import request from 'supertest-as-promised'
import express from '../../services/express'
import bookshelf from '../../services/bookshelf'
import User from '../users/model'
import routes from '.'

const app = () => express(routes)

const userContractor = {
  email: 'test@test.com',
  password: '123456',
  role: 'contractor'
}
const userConsumer = {
  email: 'test1@test.com',
  password: '123456',
  role: 'consumer'
}
const userLogin = {
  email: 'test1@test.com',
  password: '123456'
}

beforeEach(async () => {
  await bookshelf
    .knex
    .table(User.collection().tableName())
    .whereIn('email', [
      userLogin.email,
      userConsumer.email,
      userContractor.email
    ])
    .del()
})

test('POST /login 200 login', async () => {
  await request(app())
    .post('/registration')
    .send(userConsumer)

  const {status, body} = await request(app())
    .post('/login')
    .send(userLogin)
  expect(status).toBe(200)
  expect(typeof body).toBe('object')
  expect(body.email).toBe(userLogin.email)
})

test('POST /login 400 login [not found]', async () => {
  await request(app())
    .post('/registration')
    .send(userContractor)

  const {status, body} = await request(app())
    .post('/login')
    .send(userLogin)
  expect(status).toBe(400)
  expect(typeof body).toBe('object')
  expect(body.errors).toBe('User not found')
})

test('POST /login 400 login [invalid password]', async () => {
  await request(app())
    .post('/registration')
    .send(userConsumer)

  const {status, body} = await request(app())
    .post('/login')
    .send({
      ...userLogin,
      password: 'fake'
    })
  expect(status).toBe(400)
  expect(typeof body).toBe('object')
  expect(body.errors).toBe('Invalid password')
})

test('POST /registration 200 [consumer]', async () => {
  const { status, body } = await request(app())
    .post('/registration')
    .send(userConsumer)
  expect(status).toBe(200)
  expect(typeof body).toBe('object')
  expect(body.email).toBe(userConsumer.email)
})

test('POST /registration 200 [contractor]', async () => {
  const { status, body } = await request(app())
    .post('/registration')
    .send(userContractor)
  expect(status).toBe(200)
  expect(typeof body).toBe('object')
  expect(body.email).toBe(userContractor.email)
})

test('POST /registration 400', async () => {
  const {status, body} = await request(app())
    .post('/registration')
  expect(status).toBe(400)
  expect(typeof body).toBe('object')
  expect(body.errors[0].messages[0]).toBe('"email" is required')
})
