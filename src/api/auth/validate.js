import { string } from 'joi'

export const registration = {
  options: {
    allowUnknownBody: false
  },
  body: {
    email: string().email().required(),
    password: string().regex(/[a-zA-Z0-9]{3,30}/).required(),
    role: string().regex(/consumer|contractor/).required(),
    address: string()
  }
}

export const login = {
  options: {
    allowUnknownBody: false
  },
  body: {
    email: string().email().required(),
    password: string().regex(/[a-zA-Z0-9]{3,30}/).required()
  }
}
