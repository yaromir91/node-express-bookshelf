import { Router } from 'express'
import validate from 'express-validation'
import { login, registration } from './controller'
import { registration as validateRegistration, login as validateLogin } from './validate'

const router = new Router()

/**
 * @api {post} /login Login
 * @apiName Login
 * @apiGroup Auth
 * @apiParam {String} email Email
 * @apiParam {String} password Password
 * @apiSuccess (Success 200) {Object} user Current user's data.
 * @apiError 400 Validation error.
 */
router.post('/login',
  validate(validateLogin),
  login)

/**
 * @api {post} /registration Registration
 * @apiName Registration
 * @apiGroup Auth
 * @apiParam {String} email Email
 * @apiParam {String} password Password
 * @apiParam {String} role Role current user [consumer,contractor]
 * @apiSuccess (Success 200) {Object} user Current user's data.
 * @apiError 400 Validation error.
 */
router.post('/registration',
  validate(validateRegistration),
  registration)

export default router
