import request from 'supertest-as-promised'
import { address } from 'faker'
import express from '../../services/express'
import bookshelf from '../../services/bookshelf'
import Quites from '../quites/model'
import Users from '../users/model'
import routes from '.'

const app = () => express(routes)

let userTestContractor,
  userTestConsumer,
  dataQuite

beforeEach(async () => {
  await bookshelf.knex.table(Quites.collection().tableName()).del()
  userTestConsumer = await bookshelf.knex.table(Users.collection().tableName()).where('role_id', 2)
  userTestContractor = await bookshelf.knex.table(Users.collection().tableName()).where('role_id', 3)

  dataQuite = {
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, sunt.',
    to_user_id: userTestConsumer[0].id,
    address: address.streetAddress(),
    latitude: address.latitude(),
    longitude: address.longitude()
  }
})

test('POST /quites 401', async () => {
  const {text, status} = await request(app())
    .post('/quites')
    .send({})
  expect(status).toBe(401)
  expect(text).toBe('Unauthorized')
})

test('POST /quites 200', async () => {
  const {body, status} = await request(app())
    .post('/quites')
    .set('Authorization', `Bearer ${userTestContractor[0].token}`)
    .send(dataQuite)
  expect(status).toBe(200)
  expect(typeof body).toBe('object')
  expect(body.from_user_id).toBe(userTestContractor[0].id)
})

test('PATCH /quites/:id/:status 200 change quite status', async () => {
  const status = ['accepted', 'rejected']
  const result1 = await request(app())
    .post('/quites')
    .set('Authorization', `Bearer ${userTestContractor[0].token}`)
    .send(dataQuite)

  const quiteId = result1.body.id

  const result2 = await request(app())
    .patch(`/quites/${quiteId}`)
    .set('Authorization', `Bearer ${userTestContractor[0].token}`)
    .send({status: status[0]})

  expect(result2.status).toBe(200)
  expect(typeof result2.body).toBe('object')
  expect(result2.body.status).toBe(status[0])

  const result3 = await request(app())
    .patch(`/quites/${quiteId}`)
    .set('Authorization', `Bearer ${userTestContractor[0].token}`)
    .send({status: status[1]})

  expect(result3.status).toBe(200)
  expect(typeof result3.body).toBe('object')
  expect(result3.body.status).toBe(status[1])
})

test('PATCH /quites/:id/:status 404 change quite status', async () => {
  const result = await request(app())
    .patch(`/quites`)
    .set('Authorization', `Bearer ${userTestContractor[0].token}`)
    .send()

  expect(result.status).toBe(404)
  expect(typeof result.body).toBe('object')
})

test('PATCH /quites/:id/:status 400 change quite status', async () => {
  const result1 = await request(app())
    .post('/quites')
    .set('Authorization', `Bearer ${userTestContractor[0].token}`)
    .send(dataQuite)

  const quiteId = result1.body.id

  const result = await request(app())
    .patch(`/quites/${quiteId}`)
    .set('Authorization', `Bearer ${userTestContractor[0].token}`)

  expect(result.status).toBe(400)
  expect(typeof result.body).toBe('object')
})
