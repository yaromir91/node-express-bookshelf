import { number, string } from 'joi'

export const proposeQuite = {
  options: {
    allowUnknownBody: false
  },
  body: {
    description: string().required(),
    to_user_id: number().required(),
    address: string().required(),
    latitude: number().precision(8),
    longitude: number().precision(8)
  }
}

export const changeStatusQuite = {
  options: {
    allowUnknownBody: false
  },
  body: {
    status: string().required()
  }
}
