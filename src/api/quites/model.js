import bookshelf from '../../services/bookshelf'
import Users from '../users/model'

export const status = ['pending', 'accepted', 'rejected', 'expired', 'paid', 'send']

const Quite = bookshelf.Model.extend({
  tableName: 'quotes',
  from: function () {
    return this.belongsTo(Users, 'from_user_id', 'id').query({columns: ['id', 'email']})
  },
  to: function () {
    return this.belongsTo(Users, 'to_user_id', 'id')
  }
})

export default Quite
