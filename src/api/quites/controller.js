import co from 'co'
import {success} from '../../services/response/'
import Quites, {status} from '../quites/model'

export const proposeQuite = ({ body, user }, res, next) => co(function* () {
  const quite = yield new Quites({
    ...body,
    status: status[0],
    from_user_id: user.get('id')
  }).save()
  const result = yield quite.fetch({
    columns: ['status', 'from_user_id', 'to_user_id', 'description'],
    withRelated: ['from', 'to']
  })
  success(res)(result)
}).catch(next)

export const changeStatusQuite = ({ body, params, user }, res, next) => co(function* () {
  const quite = yield Quites.forge({
    id: params.id,
    to_user_id: user.get('id')
  }).save({
    status: body.status
  })

  const result = yield quite.fetch({
    columns: ['status', 'from_user_id', 'to_user_id', 'description'],
    withRelated: ['from', 'to']
  })
  success(res)(result)
}).catch(next)
