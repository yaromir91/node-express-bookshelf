import { Router } from 'express'
import validate from 'express-validation'
import { proposeQuite, changeStatusQuite } from './controller'
import { proposeQuite as validateProposeQuite, changeStatusQuite as validateChangeStatusQuite } from './validate'
import { token } from '../../services/passport'

const router = new Router()

/**
 * @api {post} /quites Create quites
 * @apiPermission token
 * @apiName Quites
 * @apiGroup Quites
 * @apiParam {String} description Description quite
 * @apiParam {String} address Address quite
 * @apiParam {String} latitude Latitude quite
 * @apiParam {String} longitude Longitude quite
 * @apiParam {Number} to_user_id Quite to consumer
 * @apiSuccess (Success 200) {Object} quite Current quite's data.
 * @apiError 401 Token access only.
 * @apiError 400 Validate data.
 */
router.post('/quites',
  token(),
  validate(validateProposeQuite),
  proposeQuite)

/**
 * @api {patch} /quites/:id Change status quites
 * @apiPermission token
 * @apiName Change status quite
 * @apiGroup Quites
 * @apiParam {Number} id
 * @apiParam {String} status
 * @apiSuccess (Success 200) {Object} quite Current quite's data.
 * @apiError 401 Token access only.
 * @apiError 404 Quite is not exist.
 * @apiError 400 Validate data.
 */
router.patch('/quites/:id',
  token(),
  validate(validateChangeStatusQuite),
  changeStatusQuite)

export default router
