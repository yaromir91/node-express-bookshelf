# Ask Jack Nodejs

See the API's [documentation](./docs/index.html).

## Commands

Before init project by default `NODE_ENV=dev` for test need run `NODE_ENV=test` (it's create table and seed for test)
```bash
npm run migrate:run # for create table to postgress
npm run seed:run # for create fake data
```
After you generate your project, these commands are available in `package.json`.

```bash
npm test # test using Jest
npm run test:unit # run unit tests
npm run test:integration # run integration tests
npm run coverage # test and open the coverage report in the browser
npm run lint # lint using ESLint
npm run dev # run the API in development mode
npm run prod # run the API in production mode
npm run docs # generate API docs
```

## Playing locally

First, you will need to install and run [PostgresDB](https://www.postgresql.org).

Then, run the server in development mode.

```bash
$ npm run dev
Express server listening on http://0.0.0.0:8888, in development mode
```

If you choose to generate the authentication API, you can start to play with it.
> Note that creating and authenticating users needs a master key (which is defined in the `.env` file)


What about this story:
> As a contractor , I expect quotes to be accepted or rejected with in 5 minutes of sending them. Otherwise the quote expires.

its may be done using `crontab` or other service as `RabbitMQ`

