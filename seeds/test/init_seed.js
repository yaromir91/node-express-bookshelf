import co from 'co'
import { address, name, random, internet, lorem } from 'faker'
import Quites from '../../src/api/quites/model'
import Users, {generateBcrypt} from '../../src/api/users/model'
import Roles, { roles as arrayRoles } from '../../src/api/roles/model'

const tableUsers = Users.collection().tableName()
const tableRoles = Roles.collection().tableName()
const tableQuotes = Quites.collection().tableName()

exports.seed = function (knex, Promise) {
  knex.raw('SET CONSTRAINTS ALL DEFERRED;')

  return co(function* () {
    yield roles(knex)
    yield users(knex)
    yield quotes(knex)
  })
}
exports.config = { transaction: true }

function users (knex) {
  return co(function* () {
    yield knex.table(tableUsers).del()
    yield knex.table(tableUsers).insert([
      {
        id: 1,
        name: name.findName(),
        email: internet.email(),
        token: generateBcrypt(internet.email()),
        password: generateBcrypt('testtest'),
        address: address.streetAddress(),
        latitude: address.latitude(),
        longitude: address.longitude(),
        role_id: 1
      },
      {
        id: 2,
        name: name.findName(),
        email: internet.email(),
        token: generateBcrypt(internet.email()),
        password: generateBcrypt('testtest'),
        address: address.streetAddress(),
        latitude: address.latitude(),
        longitude: address.longitude(),
        role_id: 2
      }, {
        id: 3,
        name: name.findName(),
        email: internet.email(),
        token: generateBcrypt(internet.email()),
        password: generateBcrypt('testtest'),
        address: address.streetAddress(),
        latitude: address.latitude(),
        longitude: address.longitude(),
        role_id: 3
      },
    ])
    console.log('Seed users.')
  })
}

function roles (knex) {
  return co(function* () {
    yield knex.table(tableRoles).del()
    yield knex.table(tableRoles).insert(arrayRoles)
    console.log('Seed roles.')
  })
}

function quotes (knex, Promise) {
  return co(function* () {
    yield knex.table(tableQuotes).del()
    const usersId = yield knex(tableUsers).select('id').whereIn('role_id', [2, 3]).map(e => e.id)

    yield knex.table(tableQuotes).insert([
      {
        id: 1,
        description: lorem.words(),
        from_user_id: random.arrayElement(usersId),
        to_user_id: random.arrayElement(usersId),
        status: random.arrayElement(['accepted', 'rejected', 'expired', 'paid', 'send']),
        address: address.streetAddress(),
        latitude: address.latitude(),
        longitude: address.longitude()
      }
    ])
    console.log('Seed quotes.')
  })
}
