# ask-jack-nodejs v0.0.0



- [Auth](#auth)
	- [Login](#login)
	- [Registration](#registration)
	
- [Quites](#quites)
	- [Change status quites](#change-status-quites)
	- [Create quites](#create-quites)
	


# Auth

## Login



	POST /login


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| email			| String			|  <p>Email</p>							|
| password			| String			|  <p>Password</p>							|

## Registration



	POST /registration


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| email			| String			|  <p>Email</p>							|
| password			| String			|  <p>Password</p>							|
| role			| String			|  <p>Role current user [consumer,contractor]</p>							|

# Quites

## Change status quites



	PATCH /quites/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| id			| Number			|  							|
| status			| String			|  							|

## Create quites



	POST /quites


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| description			| String			|  <p>Description quite</p>							|
| address			| String			|  <p>Address quite</p>							|
| latitude			| String			|  <p>Latitude quite</p>							|
| longitude			| String			|  <p>Longitude quite</p>							|
| to_user_id			| Number			|  <p>Quite to consumer</p>							|


