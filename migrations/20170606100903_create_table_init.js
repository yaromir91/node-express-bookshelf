import Quites from '../src/api/quites/model'
import Users from '../src/api/users/model'
import Roles from '../src/api/roles/model'

const tableUsers = Users.collection().tableName()
const tableRoles = Roles.collection().tableName()
const tableQuotes = Quites.collection().tableName()
/**
 *
 * @param knex
 * @param [Promise]
 * @returns {*}
 */
export function up (knex) {
  return knex.schema
    .createTableIfNotExists(tableRoles, (table) => {
      table.increments('id')
      table.string('name')
    })
    .createTableIfNotExists(tableUsers, (table) => {
      table.increments('id')
      table.string('name')
      table.string('email')
      table.string('password')
      table.string('token')
      table.string('address')
      table.float('latitude', 9, 6)
      table.float('longitude', 9, 6)
      table.integer('role_id').unsigned()
      table.timestamps(true, true)

      table.foreign('role_id').references(`${tableRoles}.id`)
      table.unique('email')
    })
    .createTableIfNotExists(tableQuotes, (table) => {
      table.increments('id')
      table.text('description')
      table.integer('from_user_id').unsigned()
      table.integer('to_user_id').unsigned()
      table.enu('status', ['accepted', 'rejected', 'expired', 'paid', 'send', 'pending'])
      table.string('address')
      table.float('latitude', 9, 6)
      table.float('longitude', 9, 6)
      table.timestamps(true, true)

      table.foreign('from_user_id').references(`${tableUsers}.id`)
      table.foreign('to_user_id').references(`${tableUsers}.id`)
    })
};

export function down (knex, Promise) {
  return knex.schema
    .table('users', (table) => {
      table.dropForeign('role_id')
    })
    .table('quotes', (table) => {
      table.dropForeign('from_user_id')
      table.dropForeign('to_user_id')
    })
    .dropTableIfExists('users')
    .dropTableIfExists('roles')
    .dropTableIfExists('quotes')
};
